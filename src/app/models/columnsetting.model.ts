export interface ColumnSetting {
    field: string;
    title: string;
    format?: string;
    filter?:string;
    editor?:string;
    type: 'text' | 'numeric' | 'boolean' | 'date';
    required?: any;
  }