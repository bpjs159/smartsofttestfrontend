export interface Detailtable {
    Id: number;
    Header:string;
    TableTypeId: number;
    dataType: string;
    format: string;
    required: boolean;
  }