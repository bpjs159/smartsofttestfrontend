import { Component, OnInit } from '@angular/core';
import { ConfigurationService } from '../../services/configuration.service';
import { Table } from '../../models/table.model'

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {
  public listTables: Array<Table> = [{ Name: 'Loading tables...', Id: null }];
    public selectedTable: Table = this.listTables[0];

  constructor(private configurationService:ConfigurationService) {
    this.configurationService.getTableData().subscribe((tables:Table[]) => {
      if(tables.hasOwnProperty("Error")){
          alert("Error de conexion:");
      }else{
        this.listTables = tables;
        this.selectedTable = this.listTables[0];
        this.configurationService.setSelectedTable(this.listTables[0].Id);
      }
    });
   }

  ngOnInit(): void {
  }

  onChangeTable(e){
    this.configurationService.setSelectedTable(e.Id);
  }

}
