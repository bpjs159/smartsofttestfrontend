import { Component, OnInit, Inject } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ConfigurationService } from '../../services/configuration.service';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';

import { map } from 'rxjs/operators';
import { ColumnSetting } from '../../models/columnsetting.model';
import { Detailtable } from '../../models/detailtable.model';

import * as moment from 'moment';

import { DialogService,DialogRef,DialogCloseResult } from '@progress/kendo-angular-dialog';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit {

  //grid configuration
  public view: GridDataResult;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    public formGroup: FormGroup;
    private editedRowIndex: number;

  //array for header
  public idTable;
  public columns: ColumnSetting[] = [];

  //result for modal confirmation
  public resultModal;
  public isEditMode:boolean = false;


  constructor(private dataService:DataService, private configurationService:ConfigurationService, private dialogService: DialogService) {     
  }

  ngOnInit(): void {

    this.configurationService.getTableSelected().subscribe(idTableSelected => {

      this.idTable = idTableSelected;

      this.configurationService.getTableDetail(idTableSelected).subscribe((detailTable:Detailtable[]) => {
        //loop to get headers

        this.columns = []

        for(let i = 0;i<detailTable.length;i++){

          let objectHeader:ColumnSetting = {
              "field":"",
              "title":"",
              "type":"text",
              "editor":"",
              "filter":"",
              "format":""
          };

            objectHeader.field = detailTable[i].Header;        
            objectHeader.title = detailTable[i].Header;
            objectHeader.required = detailTable[i].required;
            objectHeader.type = "text";

            if(detailTable[i].dataType == "Int"){
              objectHeader.type = "numeric";
              objectHeader.filter = "numeric";
              objectHeader.editor = "numeric";
            }

            if(detailTable[i].dataType == "Date"){
              objectHeader.type = "date";
              objectHeader.filter = "date";
              objectHeader.format = "{0:"+detailTable[i].format+"}";
              objectHeader.editor = "date";
            }
            this.columns.push(objectHeader);
        }        

        this.getData(idTableSelected);
      });      
    });
    
  }

  private getData(idTableSelected){
    this.dataService.getTableData(idTableSelected).subscribe(rows => {      
      this.columns.forEach((col) => {
        if(col.type == "date"){
          rows.forEach((row) => {
            Object.keys(row).forEach(function(key) {
              if(key==col.title){
                let formatExtract = col.format.replace("{0:","").replace("}","");
                formatExtract = formatExtract.replace("dd","DD");
                      
                if(row[key] != null){
                  row[key] = moment(new Date(row[key])).format(formatExtract);
                }
                
              }    
            });
          });
        }
      });

      this.view = process(rows, this.gridState);
    });
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {

    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
}

  public addHandler({sender}) {

    this.closeEditor(sender);

    let newObjectForm = {};
    this.columns.forEach((col) => {
      
      if(col.required.data[0] == 1){
        newObjectForm[col.title] = new FormControl('', Validators.required);
      }else{
        newObjectForm[col.title] = new FormControl('');
      }
    });

    this.formGroup = new FormGroup(newObjectForm);
    sender.addRow(this.formGroup);
}

public saveHandler({sender, rowIndex, formGroup, isNew}) {
  
  this.isEditMode = false;
  //add idTable
  formGroup.value.idTable = this.idTable;

  Object.keys(formGroup.value).forEach(function(key) {
    if(formGroup.value[key]=="" || formGroup.value[key] == null){
      delete formGroup.value[key];
    }    
  });

  this.columns.forEach((col) => {
    if(col.type == "date"){
      Object.keys(formGroup.value).forEach(function(key) {
        if(key==col.title){
          let formatExtract = col.format.replace("{0:","").replace("}","");
          formatExtract = formatExtract.replace("dd","DD");
          formGroup.value[key] = moment(new Date(formGroup.value[key])).format(formatExtract);
        }    
      });
    }
  })


  if(isNew){
    this.dataService.setNewRecord(formGroup.value).subscribe(result => {
      if(result['Error']){
        this.openMessage("Error",result['Error']);
      }else if(result['insertId']){
        this.openMessage("Success","Record successfully saved");
        this.getData(this.idTable);
      
        sender.editRow(0, formGroup);
      }else{
        this.openMessage("Unknowed response",JSON.stringify(result));
      }
      
    });
  }else{
    this.dataService.updateRecord(formGroup.value).subscribe(result => {
      if(result['Error']){
        this.openMessage("Error",result['Error']);
      }else if(result['affectedRows'] == '1'){
        this.openMessage("Success","Record successfully updated");
        this.getData(this.idTable);
      }else{
        this.openMessage("Unknowed response",JSON.stringify(result));
      }    
    });
  }

  

  sender.closeRow(rowIndex);
}

public editHandler({sender, rowIndex, dataItem}) {
  this.isEditMode = true;  
  this.closeEditor(sender);  

  this.columns.forEach((col) => {
    if(col.type == "date"){
      Object.keys(dataItem).forEach(function(key) {
        if(key==col.title){
          dataItem[key] = new Date(dataItem[key]);          
        }    
      });
    }
  })

  let newObjectForm = {};
    this.columns.forEach((col) => {
      if(col.required.data[0] == 1){
        newObjectForm[col.title] = new FormControl(dataItem[col.title], Validators.required);
      }else{
        newObjectForm[col.title] = new FormControl(dataItem[col.title]);
      }
    });

  this.formGroup = new FormGroup(newObjectForm);

  this.editedRowIndex = rowIndex;

  sender.editRow(rowIndex, this.formGroup);
}


  public onStateChange(state: State) {
    this.gridState = state;
  }

  public cancelHandler({sender, rowIndex}) {
      this.isEditMode = false;
      this.closeEditor(sender, rowIndex);
  }

  

  public removeHandler({dataItem}) {      
    const dialog: DialogRef = this.dialogService.open({
      title: 'Please confirm',
      content: 'Do you really want to delete the record?',
      actions: [
          { text: 'No' },
          { text: 'Yes', primary: true }
      ],
      width: 450,
      height: 200,
      minWidth: 250
  });

  dialog.result.subscribe((result) => {
      if (!(result instanceof DialogCloseResult)) {
          if(result.text == "Yes"){
                //add idtable param
                dataItem.idTable = this.idTable;



                //delete record
                this.dataService.deleteRecord(dataItem).subscribe(result => {        

                  if(result['Error']){
                    this.openMessage("Error",result['Error']);
                  }else if(result['affectedRows']){
                    this.openMessage("Success","Record has been deleted");
                    this.getData(this.idTable);
                  }else{
                    this.openMessage("Unknow response",JSON.stringify(result));
                  }
                });

                this.dataService.getTableData(this.idTable).subscribe(rows => {
                  this.view = process(rows, this.gridState);
                });
          }
          
      
      }

      this.resultModal = JSON.stringify(result);
  });
    

      
  }

  private openMessage(title, message){
      const dialog: DialogRef = this.dialogService.open({
        title: title,
        content: message,
        actions: [
            { text: 'Close' }
        ],
        width: 450,
        height: 200,
        minWidth: 250
    });
  };

   


}
