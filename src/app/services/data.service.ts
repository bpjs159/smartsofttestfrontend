import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Table } from '../models/table.model'

import * as Backend from '../../assets/config.backend.json';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  backendPoint = Backend.host + ":" + Backend.port;

  constructor(private http: HttpClient) { }

  getTableData(idTable):Observable<Table[]>{
    //Initialize Params Object
    let params = new HttpParams();

    //Begin assigning parameters
    params = params.append('idTable', idTable);

    //Send request
    
    return this.http.get<any[]>(this.backendPoint + "/api/getTableData",{ params: params });
  }

  setNewRecord(newRecord):Observable<any[]>{
    //Initialize Params Object
    let params = new HttpParams();

    //Begin assigning parameters
    Object.keys(newRecord).forEach(function(k){
      params = params.append(k, newRecord[k]);
    });
    //Send request


    
    return this.http.get<any[]>(this.backendPoint + "/api/setNewRecord",{ params: params });

  }


  updateRecord(recordUpdate):Observable<any[]>{
    //Initialize Params Object

    let params = new HttpParams();

    //Begin assigning parameters
    Object.keys(recordUpdate).forEach(function(k){
      params = params.append(k, recordUpdate[k]);
    });
    //Send request


    
    return this.http.get<any[]>(this.backendPoint + "/api/updateRecord",{ params: params });

  }


  deleteRecord(recordDelete):Observable<any[]>{
    //Initialize Params Object

    let params = new HttpParams();

    //Begin assigning parameters
    Object.keys(recordDelete).forEach(function(k){
      params = params.append(k, recordDelete[k]);
    });
    //Send request


    
    return this.http.get<any[]>(this.backendPoint + "/api/deleteRecord",{ params: params });

  }

}
