import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Table } from '../models/table.model'
import { Detailtable } from '../models/detailtable.model'

import * as Backend from '../../assets/config.backend.json';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  backendPoint = Backend.host + ":" + Backend.port;
  currentTable:number = null;
  tableObserverSelected:Subject<Table>;

  constructor(private http: HttpClient) { 
    this.tableObserverSelected = new Subject();
  }

  //HTTP Services

  getTableData():Observable<Table[]>{
    return this.http.get<Table[]>(this.backendPoint + "/api/getTables",{});
  }

  getTableDetail(idTable):Observable<Detailtable[]>{
    //Initialize Params Object
    let params = new HttpParams();

    //Begin assigning parameters
    params = params.append('idTable', idTable);

    //Send request

    return this.http.get<Detailtable[]>(this.backendPoint + "/api/getTableDetail",{ params: params });
  }

  //Local services

  getTableSelected():Observable<Table>{
    return this.tableObserverSelected;
  }

  setSelectedTable(idTable){
    this.currentTable = idTable;
    this.tableObserverSelected.next(idTable);
  }

}
